import akka.actor._
import cats.data.Validated._

// State
sealed trait State
case object DefaultState extends State

// Data
sealed trait Data
case class TerminalData(productCodes: Vector[String], productPricing: ProductPricing) extends Data

// Events
case class SetPricing(productCode: String, price: BigDecimal, volume: Integer)
case class Scan(productCode: String)
case object GetTotal
case object ClearCodes

class TerminalActor extends FSM[State, Data] {

  startWith(DefaultState, TerminalData(Vector.empty[String], ProductPricing()))

  when(DefaultState) {
    case Event(SetPricing(productCode, price, volume), data: TerminalData) =>
      ProductPricing.updatePricing(data.productPricing, productCode, price, volume) match {
        case Valid(productPricing) =>
          stay using data.copy(productPricing = productPricing)

        case Invalid(e) =>
          sender ! e
          stay
      }

    case Event(Scan(productCode), data: TerminalData) =>
      val updatedData = data.copy(productCodes = data.productCodes :+ productCode)
      stay using updatedData

    case Event(GetTotal, data: TerminalData) =>
      sender ! PricingCalculator.calculateTotal(data.productPricing, data.productCodes)
      stay
  }

  initialize()
}

object TerminalActor {
  def props() = Props(new TerminalActor())
}
