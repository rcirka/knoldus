import cats.data.Validated._
import cats.data.ValidatedNec
import cats.implicits._


object Validation {
  // Error objects
  sealed trait DomainValidation {
    def errorMessage: String
  }

  case object InvalidVolume extends DomainValidation {
    def errorMessage: String = "Quantity must be greater than zero."
  }

  case object InvalidPrice extends DomainValidation {
    def errorMessage: String = "Price must be greater than zero."
  }

  case object MissingMinimumVolume extends DomainValidation {
    def errorMessage: String = s"Missing a volume of 1"
  }

  case object MissingMinimumCount extends DomainValidation {
    def errorMessage: String = s"Missing a volume of 1"
  }

  case class InvalidProductCode(productCode: String) extends DomainValidation {
    def errorMessage: String = s"Product code '$productCode' is missing from product pricing."
  }


  // Error methods
  type ValidationResult[A] = ValidatedNec[DomainValidation, A]

  def validateMinimumVolume(prices: Seq[(Int, BigDecimal)]): ValidationResult[Seq[(Int, BigDecimal)]] = {
    if (prices.exists(_._1 == 1)) prices.validNec else MissingMinimumVolume.invalidNec
  }

  def validateMinimumCount(count: Int): ValidationResult[Int] = {
    if (count >= 0) count.validNec else MissingMinimumCount.invalidNec
  }

  def validateVolume(volume: Int): ValidationResult[Int] = {
    if (volume >= 1 ) volume.validNec else InvalidVolume.invalidNec
  }

  def validatePrice(price: BigDecimal): ValidationResult[BigDecimal] = {
    if (price >= 0 ) price.validNec else InvalidPrice.invalidNec
  }

  def validateExists(productCode: String, productPricing: ProductPricing): ValidationResult[String] = {
    if (productPricing.map.keySet.map(_._1).contains(productCode)) productCode.validNec else InvalidProductCode(productCode).invalidNec
  }

  def validateItemPricesExist(productPricing: ProductPricing, items: Seq[String]): ValidationResult[String] = {
    items
    .distinct
    .map(productCode => validateExists(productCode, productPricing))
    .toList
    .combineAll
  }
}
