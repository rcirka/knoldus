import Validation._
import cats.data.Validated._
import cats.implicits._

import scala.annotation.tailrec

object PricingCalculator {
  /**
    * Calculate the total price with supplied productPricing and items
    * @param productPricing
    * @param items
    * @return
    */
  def calculateTotal(productPricing: ProductPricing, items: Seq[String]): ValidationResult[BigDecimal] = {
    validateItemPricesExist(productPricing, items).andThen { _ =>
      val itemCounts = items.groupBy(identity).mapValues(_.length).toList

      itemCounts
      .map { case (productCode, count) => calculateTotal(count, productPricing.get(productCode)) }
      .combineAll
    }
  }

  /**
    * Calculate pricing for a single product
    * @param count
    * @param prices
    * @return
    */
  def calculateTotal(count: Int, prices: Seq[(Int, BigDecimal)]): ValidationResult[BigDecimal] = {
    (validateMinimumVolume(prices), validateMinimumCount(count))
    .mapN { case (x, y) => (x, y) }
    .andThen { case (pricesV, countV) =>
      val sortedPrices = prices.sortBy(-_._1)
      calcTotalRec(countV, sortedPrices, BigDecimal(0).validNec)
    }
  }

  /**
    * Calculate the totals recursively. Requires the prices to be pre-sorted
    * @param count
    * @param prices
    * @param total
    * @return
    */
  @tailrec
  protected def calcTotalRec(count: Int, prices: Seq[(Int, BigDecimal)], total: ValidationResult[BigDecimal]): ValidationResult[BigDecimal] = {
    if (count == 0) {
      total
    }
    else {
      prices.find(_._1 <= count) match {
        case Some((volume, price)) =>
          val buyQty = count / volume
          val purchase = buyQty * price
          val remainingCount = count - (buyQty * volume)
          calcTotalRec(remainingCount, prices, total.map(_ + purchase))

        case None =>
          MissingMinimumVolume.invalidNec
      }
    }
  }
}
