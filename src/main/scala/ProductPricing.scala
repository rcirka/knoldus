import Validation._
import cats.data.Validated._
import cats.implicits._


// Private constructor, to force validation through factory methods
class ProductPricing private(val map: Map[(String, BigDecimal), Int]) {
  def get(productId: String): Seq[(Int, BigDecimal)] = {
    map.toSeq.filter(_._1._1 == productId).map { case ((pId, price), vol) => (vol, price)}
  }
}

// Factory pattern
object ProductPricing {

  /**
    * Default constructor
    * @return
    */
  def apply(): ProductPricing = {
    new ProductPricing(Map.empty[(String, BigDecimal), Int])
  }

  /**
    * Constructor with supplied pricings
    * @param pricings
    * @return
    */
  def apply(pricings: Seq[(String, BigDecimal, Int)]): ValidationResult[ProductPricing] = {
    val init: ValidationResult[ProductPricing] = ProductPricing().validNec

    pricings.foldLeft(init){ case (productPricing, (productCode, price, volume)) =>
      productPricing.andThen(p => updatePricing(p, productCode, price, volume))
    }
  }

  /**
    * Add or update a pricing
    * @param productMap
    * @param productCode
    * @param price
    * @param volume
    * @return
    */
  def updatePricing(productMap: ProductPricing, productCode: String, price: BigDecimal, volume: Int = 0): ValidationResult[ProductPricing] = {
    (validateVolume(volume), validatePrice(price))
    .mapN { case (validVolume, validPrice) =>
      val updatedMap = productMap.map.updated((productCode, validPrice), validVolume)
      new ProductPricing(updatedMap)
    }
  }
}
