import akka.actor.ActorSystem
import akka.testkit._
import cats.data.Validated.Valid
import org.scalatest._


class TerminalActorSpec extends TestKit(ActorSystem()) with ImplicitSender with WordSpecLike with Matchers with BeforeAndAfterAll {

  override def afterAll: Unit = {
    TestKit.shutdownActorSystem(system)
  }

  "TerminalActor" must {
    "scan ABCDABAA correctly" in {
      val terminalActorRef = system.actorOf(TerminalActor.props())

      terminalActorRef ! SetPricing("A", BigDecimal(2), 1)
      terminalActorRef ! SetPricing("A", BigDecimal(7), 4)
      terminalActorRef ! SetPricing("B", BigDecimal(12), 1)
      terminalActorRef ! SetPricing("C", BigDecimal(1.25), 1)
      terminalActorRef ! SetPricing("C", BigDecimal(6), 6)
      terminalActorRef ! SetPricing("D", BigDecimal(.15), 1)

      terminalActorRef ! Scan("A")
      terminalActorRef ! Scan("B")
      terminalActorRef ! Scan("C")
      terminalActorRef ! Scan("D")
      terminalActorRef ! Scan("A")
      terminalActorRef ! Scan("B")
      terminalActorRef ! Scan("A")
      terminalActorRef ! Scan("A")

      terminalActorRef ! GetTotal
      expectMsg(Valid(BigDecimal(32.40)))
    }

    "scan CCCCCCC correctly" in {
      val terminalActorRef = system.actorOf(TerminalActor.props())

      terminalActorRef ! SetPricing("A", BigDecimal(2), 1)
      terminalActorRef ! SetPricing("A", BigDecimal(7), 4)
      terminalActorRef ! SetPricing("B", BigDecimal(12), 1)
      terminalActorRef ! SetPricing("C", BigDecimal(1.25), 1)
      terminalActorRef ! SetPricing("C", BigDecimal(6), 6)
      terminalActorRef ! SetPricing("D", BigDecimal(.15), 1)

      terminalActorRef ! Scan("C")
      terminalActorRef ! Scan("C")
      terminalActorRef ! Scan("C")
      terminalActorRef ! Scan("C")
      terminalActorRef ! Scan("C")
      terminalActorRef ! Scan("C")
      terminalActorRef ! Scan("C")

      terminalActorRef ! GetTotal
      expectMsg(Valid(BigDecimal(7.25)))
    }

    "scan ABCD correctly" in {
      val terminalActorRef = system.actorOf(TerminalActor.props())

      terminalActorRef ! SetPricing("A", BigDecimal(2), 1)
      terminalActorRef ! SetPricing("A", BigDecimal(7), 4)
      terminalActorRef ! SetPricing("B", BigDecimal(12), 1)
      terminalActorRef ! SetPricing("C", BigDecimal(1.25), 1)
      terminalActorRef ! SetPricing("C", BigDecimal(6), 6)
      terminalActorRef ! SetPricing("D", BigDecimal(.15), 1)

      terminalActorRef ! Scan("A")
      terminalActorRef ! Scan("B")
      terminalActorRef ! Scan("C")
      terminalActorRef ! Scan("D")

      terminalActorRef ! GetTotal
      expectMsg(Valid(BigDecimal(15.40)))
    }
  }
}
