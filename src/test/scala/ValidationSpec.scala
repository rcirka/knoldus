import Validation._
import cats.data.Validated._
import cats.data._
import org.scalatest._

class ValidationSpec extends WordSpec with Matchers {

  "validateItems" must {
    "return valid pricings" in {
      val productPricingNec = ProductPricing(Seq(
        ("A", BigDecimal(3), 1),
        ("B", BigDecimal(2), 1)
      ))

      val result: ValidationResult[String] = productPricingNec.andThen { productPricing =>
        validateItemPricesExist(productPricing, Seq("A", "B"))
      }

      assert(result.isValid)
    }

    "return missing product pricing" in {
      val productPricingNec = ProductPricing(Seq(
        ("A", BigDecimal(3), 1),
        ("B", BigDecimal(2), 1)
      ))

      val result: ValidationResult[String] = productPricingNec.andThen { productPricing =>
        validateItemPricesExist(productPricing, Seq("A", "B", "C"))
      }

      assert(result.isInvalid)
      assert(result == Invalid(Chain(InvalidProductCode("C"))))
    }

    "return missing product pricings" in {
      val productPricingNec = ProductPricing(Seq(
        ("A", BigDecimal(3), 1),
        ("B", BigDecimal(2), 1)
      ))

      val result: ValidationResult[String] = productPricingNec.andThen { productPricing =>
        validateItemPricesExist(productPricing, Seq("A", "B", "C", "D"))
      }

      assert(result.isInvalid)

      result match {
        case Invalid(e) =>
          assert(e.toNonEmptyList == NonEmptyList(InvalidProductCode("C"), List(InvalidProductCode("D"))))

        case Valid(a) => fail("Failed Validation")
      }
    }
  }

}
