import Validation._
import cats.data.Validated._
import cats.data._
import org.scalatest._

class ProductPricingSpec extends WordSpec with Matchers {
  "apply" must {
    "return valid items" in {
      val productPricing = ProductPricing(Seq(
        ("A", BigDecimal(3), 1),
        ("B", BigDecimal(2), 1)
      ))

      assert(productPricing.isValid)
    }

    "return errors correctly for invalid volume" in {
      val productPricing = ProductPricing(Seq(
        ("A", BigDecimal(3), 1),
        ("B", BigDecimal(3), -1)
      ))

      assert(productPricing.isInvalid)
      assert(productPricing == Invalid(Chain(InvalidVolume)))
    }

    "return errors correctly for invalid pricing" in {
      val productPricing = ProductPricing(Seq(
        ("A", BigDecimal(3), 1),
        ("B", BigDecimal(-1), 1)
      ))

      assert(productPricing.isInvalid)
      assert(productPricing == Invalid(Chain(InvalidPrice)))
    }
  }

  "updatePricing" when {
    "should apply a single update successfully" in {
      val productPricingVal = ProductPricing.updatePricing(ProductPricing(), "A", BigDecimal(2), 1)

      productPricingVal match {
        case Valid(p) =>
          assert(p.map == Map(("A", BigDecimal(2)) -> 1))
        case Invalid(e) =>
          fail("Failed Validation")
      }
    }

    "should apply multiple updates successfully" in {
      val productPricingVal = ProductPricing.updatePricing(ProductPricing(), "A", BigDecimal(2), 1)
      .andThen(p => ProductPricing.updatePricing(p, "A", BigDecimal(1), 2))
      .andThen(p => ProductPricing.updatePricing(p, "B", BigDecimal(3), 3))

      productPricingVal match {
        case Valid(p) =>
          assert(p.map == Map(
            ("A", BigDecimal(2)) -> 1,
            ("A", BigDecimal(1)) -> 2,
            ("B", BigDecimal(3)) -> 3
          ))
        case Invalid(e) =>
          fail("Failed Validation")
      }
    }
  }
}
