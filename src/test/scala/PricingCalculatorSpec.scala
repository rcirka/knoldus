import PricingCalculator._
import cats.data.Validated._
import org.scalatest._


class PricingCalculatorSpec extends WordSpec with Matchers {
  val volPrices = ProductPricing(
    Seq(
      ("A", BigDecimal(2), 1),
      ("A", BigDecimal(7), 4),
      ("B", BigDecimal(12), 1),
      ("C", BigDecimal(1.25), 1),
      ("C", BigDecimal(6), 6),
      ("D", BigDecimal(.15), 1)
    )
  ).toOption.get

  "calculateTotal" must {
    "calculate ABCDABAA" in {
      val total = calculateTotal(volPrices, "ABCDABAA".toSeq.map(_.toString))
      assert(total == Valid(BigDecimal(32.40)))
    }

    "calculate CCCCCCC" in {
      val total = calculateTotal(volPrices, "CCCCCCC".toSeq.map(_.toString))
      assert(total == Valid(BigDecimal(7.25)))
    }

    "calculate ABCD" in {
      val total = calculateTotal(volPrices, "ABCD".toSeq.map(_.toString))
      assert(total == Valid(BigDecimal(15.40)))
    }
  }

  "calcTotal" must {
    "calculate AAAA" in {
      val prices = volPrices.get("A")

      val total = calculateTotal(4, prices)
      assert(total == Valid(BigDecimal(7)))
    }

    "calculate BB" in {
      val prices = volPrices.get("B")

      val total = calculateTotal(2, prices)
      assert(total == Valid(BigDecimal(24)))
    }

    "calculate C" in {
      val prices = volPrices.get("C")

      val total = calculateTotal(1, prices)
      assert(total == Valid(BigDecimal(1.25)))
    }

    "calculate D" in {
      val prices = volPrices.get("D")

      val total = calculateTotal(1, prices)
      assert(total == Valid(BigDecimal(.15)))
    }

    "return correct price for one price" in {
      val prices = Seq((1, BigDecimal(6)))

      val total = calculateTotal(5, prices)
      assert(total == Valid(BigDecimal(30)))
    }

    "return correct price for two prices" in {
      val prices = Seq(
        (1, BigDecimal(6)),
        (3, BigDecimal(5))
      )

      val total = calculateTotal(10, prices)
      assert(total == Valid(BigDecimal(21)))
    }

    "return correct price for three prices" in {
      val prices = Seq(
        (1, BigDecimal(3)), // 30
        (2, BigDecimal(5)), // 25
        (10, BigDecimal(20)) // 20
      )

      val total = calculateTotal(15, prices)
      assert(total == Valid(BigDecimal(33)))
    }
  }
}
