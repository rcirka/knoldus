# Readme

This is a solution for the knoldus assignment using Scala.

It uses Maven for dependency management, ScalaTest for tests, Akka actor for the terminal, and Cats for validation.

To run the tests, type the following in a terminal in the root of the project directory (assuming maven is installed)
```
mvn test
```

There are 4 files with corresponding tests:  
+ Validation  
   * Used for validating inputs  
+ ProductPricing    
   *  Used for storing prices per volume    
+ PricingCalculator    
   * Used for calculating the total price  
+ TerminalActor  
   * Used as the sale terminal service object  
   * Uses a Finite State Machine to avoid vars  

As per the spec, there are no vars, nulls, or exceptions
